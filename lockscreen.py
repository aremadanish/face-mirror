import tkinter 
from tkinter import *
from tkinter import messagebox		#importing tkinter and its packages for gui
import cv2							# importing cv2 to capture image image from webcam
import face_recognition				# face_recognition library to compare faces
import numpy as np

myclient = pymongo.MongoClient("localhost",27017)		# creating a MongoDB object
mydb = myclient["testdb"]								# connecting to database
coll = mydb["employees"]								# connecting to collection

tk = tkinter.Tk() 
tk.geometry("1600x800")						# creating gui object and defining its window size

def image_click():							# function called on unlock button
	flag = False								# initializing flag to check weather there is a match or not
	count = 0									# starting counter in case multiple matches found
	cap = cv2.VideoCapture(0)					# initializing camera to take picture

	while True:
		ret, frame = cap.read()
		
		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)      # converting frame from camera to grey for easy face detection
		# 
		cv2.imshow("Image", frame)							

		face_locations = face_recognition.face_locations(gray)				# detecting face(s) in frame
		print(str(len(face_locations))+" Faces Found")						# counting no. of faces in image
		if face_locations != []:								# continue capturing until face is detected
			print("face found")
			break										# breaking out of loop if face(s) detected

	cap.release()
	cv2.destroyAllWindows()							# closing all camera windows after face(s) is/are detected

		if len(face_locations) > 1:							# if more than one faces are detected
		c = 0
		horizontal = 200									# value to place button horizontally
		vertical = 400										# value to place button vertically
		lt = []
		for location in face_locations:							
			top, right, bottom, left = location 							# getting all the face locations in captured image
			crop = frame[top:bottom,left:right]								# cropping all the faces in captured image
			crop_encoding = face_recognition.face_encodings(crop)				# encoding all the faces
			p = str(c)+".png"
			cv2.imwrite(p,crop)													# saving all the captured faces into the local directory
			for img_file in os.listdir("C:\\Users\\aaa\\Desktop\\GUI\\"):		# reading all the files in the directory
	#			print(img_file)
				if img_file.endswith(".png"):
					#print(img_file)
					lt.append(img_file)												# checking if files ends with .png and appending to the blank list
					#print(lt)
					for img in lt:											# iterating through all the image files in newly formed list
					#	print(img)
						photo = PhotoImage(file = img)							# reading image to use in tkinter GUI
						
						img_btn = Button(tk, text = "Click", image = photo)		# creating a new button for each face
						img_btn.place(x=horizontal,y=vertical)					# defining button position
						c = c+1
						horizontal = horizontal + 200							# increasing counters 

	for x in coll.find():								# reading all the documents in mongoDB
		im = (x["image"])									# getting image from the document


		known_image = face_recognition.load_image_file(im)					# loading image read from database 
		#unknown_image = face_recognition.load_image_file(frame)
		known_encoding = face_recognition.face_encodings(known_image)[0]		# encoding face in image from database
		unknown_encoding = face_recognition.face_encodings(frame)			# encoding face(s) in captured image
		for enc1 in unknown_encoding:											# iterating through all the encoding(s) in captured image
			results = face_recognition.compare_faces([known_encoding], enc1,tolerance = 0.6)		# comparing all the encodngs with each other
	
			if results == [True]:
				print(results)							
				flag = True										# setting flag to true when match is found
				count = count + 1								# counting total no. of matches
				#print(im)
				for i in coll.find({"image":im}):				# reading all the documents in database
					nm = i["name"]								# getting names of matched faces from database
					print(nm)
					print(im)
					engine = pyttsx3.init()						# creating object of of voice engine
					#engine.say("Welcome  " + str(nm))			# speaking name of matched faces
					#engine.setProperty()
					engine.runAndWait()
					imm = cv2.imread(im)							# pyttsx3 method to clear up all the text-to-speech commands
					cv2.imshow("image",imm)
					cv2.waitKey(0)
					cv2.destroyAllWindows()
#================================= SECOND SCREEN ======================================================

				
	if not flag:										# if captured image does not match with any existing image
		tk.destroy()									# new registration window will open and .destroy() method will close previous window

		tr = tkinter.Tk()									# creating new GUI object and defining its size
		tr.geometry("1600x800")

		name_label = Label(tr, text = "Full Name", font = ('Arial',15))
		name_label.place(x=400,y=200)

		name_entry = Entry(tr, font = ('Arial',15),width = 15)
		name_entry.place(x=550,y=200)

		mobile_label = Label(tr, text = "Mobile No.", font = ('Arial',15))
		mobile_label.place(x=400,y=300)

		mobile_entry = Entry(tr, font = ('Arial',15),width = 15)
		mobile_entry.place(x=550,y=300)

		des_label = Label(tr, text = "Designation", font = ('Arial',15))
		des_label.place(x=400,y=400)

		des_entry = Entry(tr, font = ('Arial',15),width = 15)
		des_entry.place(x=550,y=400)										# creating labels and entry boxes for registration

		def save():											# function called on save button in registration screen
			print('Hello')
			
			name = name_entry.get().title()
			mob = mobile_entry.get()
			pos = des_entry.get().title()
#			img_path = os.path.abspath(img_name)
			if name == '' or mob == '' or pos == '':										# getting values from entry boxes and 
				messagebox.showerror("Warning","Please fill all the details")				# checking for blank fields
			else:
				img_path = "C:\\Users\\aaa\\Desktop\\GUI\\" +name+ ".jpg"			# definig image path for new registration
				cv2.imwrite(img_path, frame)										# saving new rigisted image
#				os.rename(img_path,"C:\\Users\\aaa\\Desktop\\GUI\\" +name+ ".jpg")
				
				data = {"name":name,"mobile" :mob,"designation":pos,"image":img_path}
				s = coll.insert_one(data)													# saving data in database
		save_btn = Button(tr, text = "Register", font = ('Arial',15), command = save)
		save_btn.place(x=500,y=500)															# button to save new registration details

		tr.mainloop()												
	
	else:															
		nm_lbl = Label(tk, text=nm,font=('Arial',30))
		nm_lbl.place(x=550,y=300)
		#import home

lbl = Label(tk,text = "Welcome \n Please Press Button To Unlock",font = ('Arial',20))
lbl.place(x=450,y=00)

btn_unlock = Button(tk,text = "Unlock",font = ('Arial',15),command = image_click)
btn_unlock.place(x=600,y=600)
#image_click()
tk.mainloop()